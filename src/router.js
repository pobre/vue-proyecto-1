import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/organizaciones',
      name: 'organizaciones',
      component: () => import(/* webpackChunkName: "Otra" */ './views/Organizaciones.vue')
    },
    {
      path: '/fotos/:id',
      name: 'fotos',
      component: () => import(/* webpackChunkName: "fotos" */ './views/Fotos.vue')
    },
    {
      path: '/cortsvalencianes',
      name: 'cortsvalencianes',
      component: () => import(/* webpackChunkName: "fotos" */ './views/Cortsvalencianes.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import(/* webpackChunkName: "fotos" */ './views/Register.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "fotos" */ './views/Login.vue')
    }
  ]
})
